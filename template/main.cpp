#include "stdafx.h"
#include "resources.h"

char* addinName = "fat-free gayka";

KompasObject kompas( NULL );
void GetKompas();
KompasObject GetKompasApp()
{
    if (!kompas.m_lpDispatch)
        GetKompas();
    return kompas;
}

void GetKompas()
{
    CString filename;
  if( ::GetModuleFileName(NULL, filename.GetBuffer(255), 255) )
  {
        filename.ReleaseBuffer( 255 );
        CString libname(_T("kAPI5.dll"));

    filename.Replace( filename.Right(filename.GetLength() - (filename.ReverseFind('\\') + 1)),
                                            libname );

        HINSTANCE hAppAuto = LoadLibrary( filename ); // Идентификатор kAPI5.dll
        if(  hAppAuto )
    {
            typedef LPDISPATCH ( WINAPI *FCreateKompasObject )();
            FCreateKompasObject pCreateKompasObject =
                (FCreateKompasObject)GetProcAddress( hAppAuto, "CreateKompasObject" );
            if ( pCreateKompasObject )
                kompas = pCreateKompasObject();
            FreeLibrary( hAppAuto );
        }
    }
}

extern "C" __declspec(dllexport) char* WINAPI LIBRARYNAME()
{
    return addinName;
}

extern "C" __declspec(dllexport) unsigned int WINAPI LIBRARYID()
{
    return IDR_LIBID;
}

//-------------------------------------------------------------------------------
extern "C" __declspec(dllexport) void WINAPI LIBRARYENTRY(unsigned int id)
{
    // по умолчанию ресурсы берутся из .exe файла, для того чтобы их достать
    // из нашей dll вызовем этот define
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (GetKompasApp().m_lpDispatch)
    {
        (id);
        //KompasCommands::Execute(id);
    }
}
