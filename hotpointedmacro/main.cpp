#include "stdafx.h"
#include "resources.h"

#include <iostream>
#include <cmath>
#include <vector>
using std::vector;
#include <QDebug>

#include <globals.h>
#include <foreachmacro.h>

char* addinName = "hotpointedmacro";

extern "C" __declspec(dllexport) char* WINAPI LIBRARYNAME()
{
    return addinName;
}

extern "C" __declspec(dllexport) unsigned int WINAPI LIBRARYID()
{
    return IDR_LIBID;
}

void OnCommandEditMacro(KompasObject);
void OnCommandCreateMacro(KompasObject);

void Test1()
{
    long ok, group, macro;
    KompasObject app = GetKompasApp();
    ksDocument2D doc = app.ActiveDocument2D();
    ksUserParam mParam = app.GetParamStruct(ko_UserParam);
    mParam.Init();
    mParam.SetNumber(0xbeaf);

    ok = doc.ksMacro(0);
    group = doc.ksNewGroup(1);

    macro = doc.ksEndObj();
    assert(macro);

    //ok = doc.ksEndGroup();
    ok = doc.ksStoreTmpGroup(group);
    assert(ok);

    ksLtVariant item(app.GetParamStruct(ko_LtVariant));
    ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));
    mParam.Init();
    mParam.SetUserArray(array);

    item.Init();
    item.SetIntVal(0);
    array.ksAddArrayItem(-1, item);

    long result = doc.ksSetMacroParam(
                macro, mParam, false, true, false);
    assert(result);
    ok = doc.ksUpdateMacro(macro, group);
    assert(ok);
}

extern "C" __declspec(dllexport) void WINAPI LIBRARYENTRY(unsigned int id)
{
    qDebug("LIBRARYENTRY: id = %d", id);
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (id == COMMANDS_CREATEMACRO)
        OnCommandCreateMacro(GetKompasApp());
//        Test1();
    if (id == COMMANDS_EDITMACRO)
        OnCommandEditMacro(GetKompasApp());
}

struct Point2
{
    double x, y;
    Point2(double x = 0.0, double y = 0.0) : x(x), y(y) { }
    inline double length() const
    {
        return sqrt(x * x + y * y);
    }
    inline Point2 operator-(const Point2& that) const
    {
        return Point2(x - that.x, y - that.y);
    }
    inline Point2 operator+(const Point2& that) const
    {
        return Point2(x + that.x, y + that.y);
    }
    inline Point2 rotate90() const
    {
        return Point2(y, -x);
    }
};

struct Macro : public ILibHPObject
{
//    void init(ksDocument2D doc)
//    {
//        if (mMacro == 0)
//        {
//            if (doc.ksMacro(0))
//            {
//                mMacro = doc.ksEndObj();
//            }
//            else
//            {
//                qDebug("Macro::init(): create new macro failed");
//            }
//        }
//    }

    Macro() :
        mMacro(0),
        mGroup(0),
        //mAngle(acos(-1.0) / 6.0),
        mCount(0)
    {
        long ok;
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();
        mParam = app.GetParamStruct(ko_UserParam);
        mParam.Init();
        mParam.SetNumber(0xbeaf);

        ok = doc.ksMacro(0);
        Q_ASSERT(ok);
        mMacro = doc.ksEndObj();
        Q_ASSERT(mMacro);

        mParam.Init();
        mParam.SetNumber(0xbeaf);

        //qDebug("saveMacroParam(): ksSetMacroParam");
        long result = doc.ksSetMacroParam(
                    mMacro, mParam, false, true, false);
        Q_ASSERT(result);
    }

    Macro(reference id) :
        mMacro(id),
        mGroup(0),
//        mAngle(0.0),
        mCount(0)
    {
        long ok(0);
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();
        //init(doc);

        mParam = app.GetParamStruct(ko_UserParam);
        mParam.Init();
        ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));
        ksLtVariant item(app.GetParamStruct(ko_LtVariant));
        item.Init();
        item.SetIntVal(0);
        array.ksAddArrayItem(-1, item);
        mParam.SetUserArray(array);

        if (doc.ksGetMacroParam(mMacro, mParam) == 1)
        {
            array.ksGetArrayItem(0, item);
            int size = item.GetIntVal();

            //ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));
            item.Init();
            item.SetIntVal(0);

            for (int i(0); i < size; ++i)
            {
                item.Init();
                item.SetDoubleVal(0.0);
                array.ksAddArrayItem(-1, item);
                item.Init();
                item.SetDoubleVal(0.0);
                array.ksAddArrayItem(-1, item);
            }

            mParam.SetUserArray(array);

            if (doc.ksGetMacroParam(mMacro, mParam) == 1)
            {
                for (int i(0); i < size; ++i)
                {
                    qDebug("Macro(reference): %d", i);
                    double x, y;
                    ok = array.ksGetArrayItem(1 + i * 2, item);
                    Q_ASSERT(ok);
                    x = item.GetDoubleVal();

                    ok = array.ksGetArrayItem(2 + i * 2, item);
                    Q_ASSERT(ok);
                    y = item.GetDoubleVal();

                    mPoints.push_back(Point2(x, y));
                }
            }
        }
        else
        {
            qDebug("Macro(long): get macro param failed: %s",
                   static_cast<const wchar_t*>(app.ksStrResult()));
        }

        //draw();
    }

    virtual ~Macro()
    {
        qDebug("~Macro(): ");
        //saveMacroParam();
    }

    void saveMacroParam()
    {
        qDebug("saveMacroParam()");
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();
//        init(doc);

        mParam = app.GetParamStruct(ko_UserParam);
        ksLtVariant item(app.GetParamStruct(ko_LtVariant));
        ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));
        mParam.Init(); // Init() в конструкторе Macro
        mParam.SetNumber(0xbeaf);
        mParam.SetUserArray(array);

        item.Init();
        item.SetIntVal((int)mPoints.size());
        array.ksAddArrayItem(-1, item);

        for (size_t i(0); i < mPoints.size(); ++i)
        {
            item.Init();
            item.SetDoubleVal(mPoints[i].x);
            array.ksAddArrayItem(-1, item);

            item.Init();
            item.SetDoubleVal(mPoints[i].y);
            array.ksAddArrayItem(-1, item);
        }

        qDebug("saveMacroParam(): ksSetMacroParam(%08X, ...)", mMacro);
        long result = doc.ksSetMacroParam(
                    mMacro, mParam, false, true, false);
        if (!result)
            qDebug("saveMacroParam(): ksSetMacroParam failed %08X", mMacro);
        else
            qDebug("saveMacroParam(): ksSetMacroParam ok");
    }

    void draw()
    {
        long ok(0);
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();

        mGroup = doc.ksNewGroup(1);
        Q_ASSERT(!mPoints.empty());
        if (!mPoints.empty())
        {
            for (size_t i(0); i < mPoints.size() - 1; ++i)
            {
                doc.ksLineSeg(mPoints[i].x, mPoints[i].y,
                              mPoints[i + 1].x, mPoints[i + 1].y, 1);
            }
        }
        ok = doc.ksEndGroup();
        Q_ASSERT(ok);

        ok = doc.ksUpdateMacro(mMacro, mGroup); // Теперь в макро только окружность
        Q_ASSERT(ok);

        ok = doc.ksClearGroup(mGroup, 0);
        Q_ASSERT(ok);

        ok = doc.ksDeleteObj(mGroup);
        Q_ASSERT(ok);


//        group1 = doc.ksNewGroup(1);
//        doc.ksСircle( 0, 0, 50, 1);
//        doc.ksEndGroup();
//        doc.ksUpdateMacro( macro, group1 ); // Теперь в макро только окружность
//        doc.ksClearGroup( group1, 0);
//        doc.ksDeleteObj( group1 );

        qDebug("draw(): end");
    }

    STDMETHOD_(unsigned long, AddRef)(THIS)
    {
        qDebug("AddRef: %d", mCount);
        mCount++;
        return mCount;
    }
    STDMETHOD_(unsigned long, Release)(THIS)
    {
        qDebug("Release: %d", mCount);
        mCount--;
        unsigned long rez = mCount;
        if (!mCount)
        {
            delete this;
        }
        return rez;
    }
    STDMETHOD(QueryInterface)(THIS_ REFIID riid, void FAR* FAR* ppvObj)
    {
        if (riid == IID_ILibHPObject)
        {
            *ppvObj = this;
            return S_OK;
        }

        *ppvObj = NULL;
        return E_NOINTERFACE;
    }

    STDMETHOD_(BOOL, LibHotPnt_Prepare)(THIS_ int /*index*/)
    {
        qDebug("LibHotPnt_Prepare");
        return true;
    }
    STDMETHOD_(BOOL, LibHotPnt_Complete)(THIS_ int /*index*/, BOOL /*success*/)
    {
        qDebug("LibHotPnt_Complete");
        saveMacroParam();
        draw();
        return true;
    }
    STDMETHOD_(BOOL, LibHotPnt_Get)(THIS_ HotPointDescription* point, int index)
    {
        qDebug("LibHotPnt_Get: index == %d; mPoints.size() == %d", index, mPoints.size());
        if (index >= 0 && index < mPoints.size())
        {
            point->x = mPoints[index].x;
            point->y = mPoints[index].y;
            return true;
        }
        return false;
    }

    STDMETHOD_(BOOL, LibHotPnt_Set)(THIS_ HotPointDescription* point, int index)
    {
        qDebug("LibHotPnt_Set: begin");
        if (index >= 0 && index < mPoints.size())
        {
            qDebug("LibHotPnt_Set: x=%.3lf y=%.3lf", point->x, point->y);
            mPoints[index] = /*mPoints[index] + */Point2(point->x, point->y);
//            draw();
            return true;
        }
        return false;
    }

    STDMETHOD_(BOOL, LibHotPnt_GetCursorText)(THIS_ int /*index*/, char** /*text*/) { return false; }
    STDMETHOD_(int, LibHotPnt_GetMenu)(THIS) { return 0; }
    STDMETHOD_(BOOL, LibHotPnt_ExecuteCommand)(THIS_ int /*id*/) { return false; }

    Macro& addPoint(const Point2& p)
    {
        mPoints.push_back(p);
        return *this;
    }

    inline Point2 point(int index) const
    {
        return mPoints[index];
    }

    inline int count() const
    {
        return static_cast<int>(mPoints.size());
    }

    long id() const
    {
        return mMacro;
    }

private:
    //double mAngle;

    vector<Point2> mPoints;

    long mCount; // refs count
    long mMacro;
    long mGroup;
    ksUserParam mParam;
};

void OnCommandCreateMacro(KompasObject kompas)
{   
//    auto app = GetKompasApp();
//    ksDocument2D doc = app.ActiveDocument2D();

//    ksLtVariant item(app.GetParamStruct(ko_LtVariant));
//    ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));

//    item.Init();
//    item.SetDoubleVal(42.0);
//    array.ksAddArrayItem(-1, item);

//    ksUserParam p = app.GetParamStruct(ko_UserParam);
//    p.Init();
//    p.SetNumber(0xbeaf);
//    p.SetUserArray(array);



//    doc.ksMacro(0);
//    long id = doc.ksEndObj();

//    //qDebug("id = %08X", id);

//    if (doc.ksSetMacroParam(id, p, false, false, false) == 0)
//    {
//        qWarning("ksSetMacroParam failed");
//    }

    qDebug("OnCommandCreateMacro:");
    Macro m;
    m.addPoint(Point2(0, 0));
    m.addPoint(Point2(100, 0));
    m.addPoint(Point2(200, 100));
    m.addPoint(Point2(300, 0));
    m.addPoint(Point2(400, 100));

    m.saveMacroParam();
    m.draw();

    long id = m.id();
    qDebug("OnCommandCreateMacro: id = %08X", id);
}

bool b(false);

void OnCommandEditMacro(KompasObject kompas)
{
    auto app = GetKompasApp();
    ksDocument2D doc = app.ActiveDocument2D();


    foreachmacro(app, doc, [](KompasObject k, ksDocument2D d, reference r)
    {
        qDebug("OnCommandEditMacro(): macro id = %08X", r);
//        ksLtVariant item(k.GetParamStruct(ko_LtVariant));
//        ksDynamicArray array(k.GetDynamicArray(LTVARIANT_ARR));

//        item.Init();
//        item.SetDoubleVal(0.0);
//        array.ksAddArrayItem(-1, item);

        ksUserParam p = k.GetParamStruct(ko_UserParam);
        p.Init();
        if (d.ksGetMacroParam(r, p) == 0)
        {
            qWarning("ksGetMacroParam failed %d", k.ksReturnResult());
            return;
        }

        qDebug("OnCommandEditMacro(): p.GetNumber() %08X", p.GetNumber());
        if (p.GetNumber() == 0xbeaf)
        {
            qDebug("got our beaf");

            Macro m(r);
            m.addPoint(m.point(m.count() - 1) + Point2(100 * b, 100 * (!b)));
            b = !b;

            m.saveMacroParam();
            m.draw();

        }
    });

//    qDebug("OnCommandEditMacro: ");
//    qDebug("OnCommandEditMacro: id = %08X", id);
//    Macro m(id);
//    m.setAngle(m.angle() + acos(-1.0) / 18.0);
//    m.draw();
}

extern "C" __declspec(dllexport) void __stdcall
LibObjInterfaceEntry(int idType, unsigned int Comm, ILibHPObject** object)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    qDebug("LibObjInterfaceEntry: begin: idType=%d Comm=%08X", idType, Comm);

    if (object)
    {
        auto kompas = GetKompasApp();
        ksDocument2D doc = kompas.ActiveDocument2D();
        if (idType == 1)
        {
            switch (Comm)
            {
            case 0xbeaf:
                qDebug("LibObjInterfaceEntry: ");
                auto macro = new Macro(doc.ksEditMacroMode());
                *object = macro;
                (*object)->AddRef();
                //macro->draw();

                break;
            }
        }
    }

    qDebug("LibObjInterfaceEntry: end");
}
