TEMPLATE = lib
#CONFIG += dll
TARGET = hotpointedmacro
#CONFIG -= app_bundle
CONFIG += qt
QT += core

RC_FILE = hotpointedmacro.rc2

KOMPASSDK = d:/15/
VPATH += $${KOMPASSDK}Include
VPATH += $${PWD}/../common/
DEFINES += _AFXDLL

INCLUDEPATH += "$${KOMPASSDK}Include/" "$${PWD}" "$${PWD}/../common/"
SOURCES += main.cpp \
    $${KOMPASSDK}Include/kapi5.cpp \
    foreachmacro.cpp \
    globals.cpp

HEADERS += \
    stdafx.h \
    resources.h \
    foreachmacro.h \
    globals.h

OTHER_FILES += \
    hotpointedmacro.rc2
