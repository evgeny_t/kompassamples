#include "stdafx.h"
#include "resources.h"

#include <iostream>
#include <cmath>
#include <QDebug>

#include <foreachmacro.h>

char* addinName = "macrosample";

// TODO: use func from common
KompasObject kompas( NULL );

void GetKompas()
{
    CString filename;
    if (::GetModuleFileName(NULL, filename.GetBuffer(255), 255))
    {       filename.ReleaseBuffer(255);
        CString libname(_T("kAPI5.dll"));
        filename.Replace(filename.Right(filename.GetLength() - (filename.ReverseFind('\\') + 1)),
                          libname);

        HINSTANCE hAppAuto = LoadLibrary(filename);
        if (hAppAuto)
        {
            typedef LPDISPATCH (WINAPI *FCreateKompasObject)();
            FCreateKompasObject pCreateKompasObject =
                    (FCreateKompasObject)GetProcAddress(hAppAuto, "CreateKompasObject");
            if (pCreateKompasObject)
                kompas = pCreateKompasObject();
            FreeLibrary(hAppAuto);
        }
    }
}

KompasObject GetKompasApp()
{
    if (!kompas.m_lpDispatch)
        GetKompas();
    return kompas;
}

CString getDllFileName()
{
    HMODULE hm;
    if (GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR)&getDllFileName,
        &hm))
    {
        TCHAR path[MAX_PATH];
        GetModuleFileName(hm, path, sizeof(path));
        return CString(path);
    }
    else
    {
        return CString();
    }
}

extern "C" __declspec(dllexport) char* WINAPI LIBRARYNAME()
{
    return addinName;
}

extern "C" __declspec(dllexport) unsigned int WINAPI LIBRARYID()
{
    return IDR_LIBID;
}

void OnCommandEditMacro(KompasObject);
void OnCommandCreateMacro(KompasObject);

extern "C" __declspec(dllexport) void WINAPI LIBRARYENTRY(unsigned int id)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (GetKompasApp().m_lpDispatch)
    {
        if (id == COMMANDS_CREATEMACRO)
            OnCommandCreateMacro(GetKompasApp());
        if (id == COMMANDS_EDITMACRO)
            OnCommandEditMacro(GetKompasApp());
    }
}

struct Point2
{
    double x, y;
    Point2(double x = 0.0, double y = 0.0) : x(x), y(y) { }
    inline double length() const
    {
        return sqrt(x * x + y * y);
    }
    inline Point2 operator-(const Point2& that) const
    {
        return Point2(x - that.x, y - that.y);
    }
    inline Point2 operator+(const Point2& that) const
    {
        return Point2(x + that.x, y + that.y);
    }
    inline Point2 rotate90() const
    {
        return Point2(y, -x);
    }
};

struct Macro
{
    Macro() :
        mMacro(0),
        mGroup(0),
        mAngle(acos(-1.0) / 6.0)
    {
        auto app = GetKompasApp();

        mParam = app.GetParamStruct(ko_UserParam);
        mParam.Init();

        draw();
        //saveMacroParam();
    }

    Macro(reference id) :
        mMacro(id),
        mGroup(0),
        mAngle(0.0)
    {
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();

        mParam = app.GetParamStruct(ko_UserParam);
        app.ksMessageBoxResult();
        mParam.Init();

        if (doc.ksGetMacroParam(mMacro, mParam) == 1)
        {
            ksLtVariant item(app.GetParamStruct(ko_LtVariant));
            item.Init();

            ksDynamicArray array(mParam.GetUserArray());
            array.ksGetArrayItem(0, item);
            mAngle = item.GetDoubleVal();
        }
        else
        {
            qDebug("Macro(long): get macro param failed: %s",
                   static_cast<const wchar_t*>(app.ksStrResult()));
        }

        draw();
    }

    ~Macro()
    {
        qDebug("~Macro(): ");
        saveMacroParam();
    }

    void saveMacroParam()
    {
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();

        ksLtVariant item(app.GetParamStruct(ko_LtVariant));
        ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));
        mParam.Init();
        mParam.SetUserArray(array);

        item.Init();
        item.SetDoubleVal(mAngle);
        array.ksAddArrayItem(-1, item);

        array.ksAddArrayItem(-1, item);

        qDebug("saveMacroParam(): mMacro: %08X", mMacro);
        if (!doc.ksExistObj(mMacro))
            qDebug("macro doesnt exist");
        doc.ksSetMacroParam(mMacro, mParam, false, false, false);
        app.ksMessageBoxResult();
        qDebug("saveMacroParam(): end");
    }

    void draw()
    {
        qDebug("draw(): ");
        auto app = GetKompasApp();
        ksDocument2D doc = app.ActiveDocument2D();

        if (mGroup)
        {
            qDebug("draw(): ksDeleteObj: %08X", mGroup);
            doc.ksDeleteObj(mGroup);
        }

        mGroup = doc.ksNewGroup(0);
        if (mMacro == 0)
            doc.ksMacro(0);

        double t(0.0);
        double step = acos(-1.0) / 18.0;
        double rad = 0.5;
        while (t + step <= mAngle + DBL_EPSILON)
        {
            doc.ksLineSeg(
                        rad * cos(t), rad * sin(t),
                        rad * cos(t + step), rad * sin(t + step),
                        1);
            doc.ksLineSeg(rad * cos(t), rad * sin(t),
                          (rad + 0.1) * cos(t), (rad + 0.1) * sin(t),
                          1);
            t += step;
        }

        doc.ksLineSeg(rad * cos(t), rad * sin(t),
                      (rad + 0.1) * cos(t), (rad + 0.1) * sin(t),
                      1);

        if (mMacro == 0)
            mMacro = doc.ksEndObj();
        qDebug("draw(): ksEndObj: %08X", mMacro);
        doc.ksEndGroup();
    }

    double angle() const
    {
        return mAngle;
    }

    void setAngle(double a)
    {
        mAngle = a;
    }

    long id() const
    {
        return mMacro;
    }
private:
    double mAngle;

    long mMacro;
    long mGroup;
    ksUserParam mParam;
};

void OnCommandCreateMacro(KompasObject kompas)
{   
    auto app = GetKompasApp();
    ksDocument2D doc = app.ActiveDocument2D();

    ksLtVariant item(app.GetParamStruct(ko_LtVariant));
    ksDynamicArray array(app.GetDynamicArray(LTVARIANT_ARR));

    item.Init();
    item.SetDoubleVal(42.0);
    array.ksAddArrayItem(-1, item);

    ksUserParam p = app.GetParamStruct(ko_UserParam);
    p.Init();
    p.SetNumber(0xbeaf);
    p.SetUserArray(array);



    doc.ksMacro(0);
    long id = doc.ksEndObj();

    //qDebug("id = %08X", id);

    if (doc.ksSetMacroParam(id, p, false, false, false) == 0)
    {
        qWarning("ksSetMacroParam failed");
    }

//    qDebug("OnCommandCreateMacro:");
//    Macro m;
//    id = m.id();
//    qDebug("OnCommandCreateMacro: id = %08X", id);
}

void OnCommandEditMacro(KompasObject kompas)
{
    auto app = GetKompasApp();
    ksDocument2D doc = app.ActiveDocument2D();


    foreachmacro(app, doc, [](KompasObject k, ksDocument2D d, reference r)
    {
        qDebug("macro id = %08X", r);
        ksLtVariant item(k.GetParamStruct(ko_LtVariant));
        ksDynamicArray array(k.GetDynamicArray(LTVARIANT_ARR));

        item.Init();
        item.SetDoubleVal(0.0);
        array.ksAddArrayItem(-1, item);

        ksUserParam p = k.GetParamStruct(ko_UserParam);
        if (p.GetNumber() == 0xbeaf)
        {
            qDebug("got our beaf");
        }

        p.Init();
        p.SetUserArray(array);

        if (d.ksGetMacroParam(r, p) == 0)
        {
            qWarning("ksGetMacroParam failed %d", k.ksReturnResult());
            return;
        }

        item.Init();
        array.ksGetArrayItem(0, item);
        qDebug("item: %.1lf", item.GetDoubleVal());
    });

//    qDebug("OnCommandEditMacro: ");
//    qDebug("OnCommandEditMacro: id = %08X", id);
//    Macro m(id);
//    m.setAngle(m.angle() + acos(-1.0) / 18.0);
//    m.draw();
}
