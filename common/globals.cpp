#include "globals.h"

KompasObject kompas( NULL );

void GetKompas()
{
    CString filename;
    if (::GetModuleFileName(NULL, filename.GetBuffer(255), 255))
    {       filename.ReleaseBuffer(255);
        CString libname(_T("kAPI5.dll"));
        filename.Replace(filename.Right(filename.GetLength() - (filename.ReverseFind('\\') + 1)),
                          libname);

        HINSTANCE hAppAuto = LoadLibrary(filename);
        if (hAppAuto)
        {
            typedef LPDISPATCH (WINAPI *FCreateKompasObject)();
            FCreateKompasObject pCreateKompasObject =
                    (FCreateKompasObject)GetProcAddress(hAppAuto, "CreateKompasObject");
            if (pCreateKompasObject)
                kompas = pCreateKompasObject();
            FreeLibrary(hAppAuto);
        }
    }
}

KompasObject GetKompasApp()
{
    if (!kompas.m_lpDispatch)
        GetKompas();
    return kompas;
}

CString getDllFileName()
{
    HMODULE hm;
    if (GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR)&getDllFileName,
        &hm))
    {
        TCHAR path[MAX_PATH];
        GetModuleFileName(hm, path, sizeof(path));
        return CString(path);
    }
    else
    {
        return CString();
    }
}
