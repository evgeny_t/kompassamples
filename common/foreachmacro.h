#pragma once

#include "globals.h"

template<typename Functor>
void foreachobj(KompasObject kompas, ksDocument2D document, long objtype, Functor functor)
{
    ksIterator iterator = kompas.GetIterator();
    if (iterator.ksCreateIterator(objtype, 0))
    {
        long ref = iterator.ksMoveIterator(_T("F"));
        do
        {
            if (ref != 0)
            {
                functor(kompas, document, ref);
            }
            ref = iterator.ksMoveIterator(_T("N"));
        } while (ref != 0);

        iterator.ksDeleteIterator();
    }
}

template<typename Functor>
void foreachmacro(KompasObject kompas, ksDocument2D document, Functor functor)
{
    foreachobj(kompas, document, MACRO_OBJ, functor);
}
