#include "stdafx.h"
#include "resources.h"

#include "globals.h"

char* addinName = "text styles sample";

extern "C" __declspec(dllexport) char* WINAPI LIBRARYNAME()
{
    return addinName;
}

extern "C" __declspec(dllexport) unsigned int WINAPI LIBRARYID()
{
    return IDR_LIBID;
}

void onCommandCreateText1(KompasObject);

extern "C" __declspec(dllexport) void WINAPI LIBRARYENTRY(unsigned int id)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (GetKompasApp().m_lpDispatch)
    {
        if (id == COMMANDS_CREATETEXT1)
            onCommandCreateText1(GetKompasApp());
    }
}

#include <QDebug>

void getAllTextStyles(KompasObject kompas, ksDocument2D doc)
{
    ksTextStyleParam textparam(kompas.GetParamStruct(ko_TextStyleParam));
    const wchar_t* defaultStyle(NULL);
    short styleId(0);
    while (1)
    {
        doc.ksGetStyleParam(TEXT_STYLE, styleId, textparam);
        if (defaultStyle && textparam.GetName() == defaultStyle)
            break;
        if (defaultStyle == NULL)
            defaultStyle = textparam.GetName();
        qDebug() << "getAllTextStyles: "
                 << QString::fromWCharArray(textparam.GetName());
        ++styleId;
    }
}

void onCommandCreateText1(KompasObject kompas)
{
    ksDocument2D doc = kompas.ActiveDocument2D();
    getAllTextStyles(kompas, doc);

    //doc.DetachDispatch();
}
