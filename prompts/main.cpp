#include "stdafx.h"
#include "resources.h"

char* addinName = "line segment prompt sample";

// TODO: use func from common
KompasObject kompas( NULL );

void GetKompas()
{
    CString filename;
    if (::GetModuleFileName(NULL, filename.GetBuffer(255), 255))
    {       filename.ReleaseBuffer(255);
        CString libname(_T("kAPI5.dll"));
        filename.Replace(filename.Right(filename.GetLength() - (filename.ReverseFind('\\') + 1)),
                          libname);

        HINSTANCE hAppAuto = LoadLibrary(filename);
        if (hAppAuto)
        {
            typedef LPDISPATCH (WINAPI *FCreateKompasObject)();
            FCreateKompasObject pCreateKompasObject =
                    (FCreateKompasObject)GetProcAddress(hAppAuto, "CreateKompasObject");
            if (pCreateKompasObject)
                kompas = pCreateKompasObject();
            FreeLibrary(hAppAuto);
        }
    }
}

KompasObject GetKompasApp()
{
    if (!kompas.m_lpDispatch)
        GetKompas();
    return kompas;
}

CString getDllFileName()
{
    HMODULE hm;
    if (GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR)&getDllFileName,
        &hm))
    {
        TCHAR path[MAX_PATH];
        GetModuleFileName(hm, path, sizeof(path));
        return CString(path);
    }
    else
    {
        return CString();
    }
}

extern "C" __declspec(dllexport) char* WINAPI LIBRARYNAME()
{
    return addinName;
}

extern "C" __declspec(dllexport) unsigned int WINAPI LIBRARYID()
{
    return IDR_LIBID;
}

void OnCommandPromptPoint(KompasObject);

extern "C" __declspec(dllexport) void WINAPI LIBRARYENTRY(unsigned int id)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (GetKompasApp().m_lpDispatch)
    {
        if (id == COMMANDS_PROMPTPOINT)
            OnCommandPromptPoint(GetKompasApp());
    }
}

struct Point2
{
    double x, y;
    Point2(double x = 0.0, double y = 0.0) : x(x), y(y) { }
    inline double length() const
    {
        return sqrt(x * x + y * y);
    }
    inline Point2 operator-(const Point2& that) const
    {
        return Point2(x - that.x, y - that.y);
    }
    inline Point2 operator+(const Point2& that) const
    {
        return Point2(x + that.x, y + that.y);
    }
    inline Point2 rotate90() const
    {
        return Point2(y, -x);
    }
};

long findNearestLineSeg(KompasObject kompas, ksDocument2D document, double x, double y)
{
    ksIterator iterator = kompas.GetIterator();
    if (!iterator.ksCreateIterator(LINESEG_OBJ, 0))
    {
        return 0;
    }

    Point2 current(x, y);
    Point2 found(DBL_MAX, DBL_MAX);
    long foundRef(0);
    long ref = iterator.ksMoveIterator(_T("F"));
    do
    {
        if (ref != 0)
        {
            ksLineSegParam param = kompas.GetParamStruct(ko_LineSegParam);
            if (document.ksGetObjParam(ref, param, ALLPARAM))
            {
                Point2 first(param.GetX1(), param.GetY1()),
                        second(param.GetX2(), param.GetY2());
                if ((found - current).length() > (first - current).length())
                {
                    found = first;
                    foundRef = ref;
                }
                if ((found - current).length() > (second - current).length())
                {
                    found = second;
                    foundRef = ref;
                }
            }
        }
        ref = iterator.ksMoveIterator(_T("N"));
    } while (ref != 0);

    iterator.ksDeleteIterator();
    return foundRef;
}

long cursorCallbackResultRef(0);

extern "C" __declspec(dllexport)
int WINAPI CursorCallback(
        int /*comm*/, double* x, double* y,
        LPDISPATCH rInfo, LPDISPATCH rPhan, int dynamic)
{
    ksDocument2D docActive = kompas.ActiveDocument2D();
    ksRequestInfo info(rInfo);
    ksPhantom phan(rPhan);

    ksType1 t1(phan.GetPhantomParam());

    if (t1.GetGr())
    {
        docActive.ksDeleteObj(t1.GetGr());
    }

    t1.SetGr(docActive.ksNewGroup(1));

    Point2 placement(*x, *y);
    long ref = findNearestLineSeg(kompas, docActive, *x, *y);
    if (ref && dynamic == 0)
    {
        cursorCallbackResultRef = ref;
        return 0;
    }

    if (ref)
    {
        ksLineSegParam param = kompas.GetParamStruct(ko_LineSegParam);
        if (docActive.ksGetObjParam(ref, param, ALLPARAM))
        {
            Point2 first(param.GetX1(), param.GetY1()),
                    second(param.GetX2(), param.GetY2());
            Point2 t(5, 5);
            for (int i(0); i < 4; ++i)
            {
                Point2 t1 = first + t - placement,
                        t2 = first + t.rotate90() - placement;
                docActive.ksLineSeg(t1.x, t1.y, t2.x, t2.y, 1);
                t = t.rotate90();
            }

            t = Point2(5, 5);
            for (int i(0); i < 4; ++i)
            {
                Point2 t1 = second + t - placement,
                        t2 = second + t.rotate90() - placement;
                docActive.ksLineSeg(t1.x, t1.y, t2.x, t2.y, 1);
                t = t.rotate90();
            }
        }
    }

    docActive.ksEndGroup();

    info.DetachDispatch();
    phan.DetachDispatch();
    return dynamic == 0 ? 0 : 1;
}

long LineSegmentPrompt(KompasObject kompas)
{
    ksDocument2D doc = kompas.ActiveDocument2D();
    ksPhantom phantom = kompas.GetParamStruct(ko_Phantom);

    phantom.Init();
    phantom.SetPhantom(1);
    ksType1 type1 = phantom.GetPhantomParam();
    assert(type1);

    type1.Init();

    type1.SetGr(doc.ksNewGroup(1));
    doc.ksCircle(0, 0, 20, 1);
    doc.ksEndGroup();

    ksRequestInfo info = kompas.GetParamStruct(ko_RequestInfo);
    assert(info);

    long handle = (long)::GetModuleHandleW(getDllFileName());
    info.Init();
    info.SetDynamic(1);
    info.SetCallBackC(_T("CursorCallback"), handle, 0);
    double x(0.0), y(0.0);
    cursorCallbackResultRef = 0;
    doc.ksCursor(info, &x, &y, phantom);
    return cursorCallbackResultRef;
}

void OnCommandPromptPoint(KompasObject kompas)
{
    ksDocument2D doc = kompas.ActiveDocument2D();
    long lineRef = LineSegmentPrompt(kompas);
    if (lineRef)
    {
        doc.ksLightObj(lineRef, 1);
    }
}
