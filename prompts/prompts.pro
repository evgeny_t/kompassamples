TEMPLATE = lib
CONFIG += dll
CONFIG -= app_bundle
CONFIG -= qt

RC_FILE = prompts.rc2

KOMPASSDK = d:/15/
VPATH += $${KOMPASSDK}Include
DEFINES += _AFXDLL

INCLUDEPATH += "$${KOMPASSDK}Include/" "$${PWD}"
SOURCES += main.cpp $${KOMPASSDK}Include/kapi5.cpp

HEADERS += \
    stdafx.h \
    resources.h

OTHER_FILES += \
    prompts.rc2

